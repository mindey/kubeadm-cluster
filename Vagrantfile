# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'
default_box = 'generic/debian11'
NODES = [
  { :hostname => "master", :ip => "192.168.56.120", :apiport => 6443 },
  { :hostname => "worker1",   :ip => "192.168.56.121", :apiport => 6444 },
  { :hostname => "worker2",   :ip => "192.168.56.122", :apiport => 6445 }
]

Vagrant.configure(2) do |config|
  NODES.each do |k8s_node|
     config.vm.define k8s_node[:hostname] do |node_config|
       node_config.vm.box = default_box
       node_config.vm.hostname = k8s_node[:hostname]
       node_config.vm.synced_folder ".", "/vagrant", type:"virtualbox"
       node_config.vm.network "private_network", ip:k8s_node[:ip], virtualbox__intnet: true
       node_config.vm.network "forwarded_port", guest: 6443, host: k8s_node[:apiport] # ACCESS K8S API
       if k8s_node[:hostname] == 'master'
          for p in 30000..30010
            node_config.vm.network "forwarded_port", guest: p, host: p, protocol: "tcp"
          end
       end         
       node_config.vm.provider "virtualbox" do |v|
         if k8s_node[:hostname] == 'master'
            v.memory = "6144"
            v.name = k8s_node[:hostname]
         else    
            v.memory = "4096"
            v.name = k8s_node[:hostname]
         end
       end  
       node_config.vm.provision "ansible" do |ansible|
         ansible.playbook="ansible-playbooks/playbooks/kubernetes-containerd.yml"
       end
       node_config.vm.provision "ansible" do |ansible|
         if k8s_node[:hostname] == 'master'
            ansible.playbook="ansible-playbooks/playbooks/deploy-kubernetes-master.yml"
            ansible.extra_vars = {
              APISERVER: "#{k8s_node[:ip]}",
              APIPORT: "#{k8s_node[:apiport]}"
            }
         else
            ansible.playbook="ansible-playbooks/playbooks/deploy-kubernetes-workers.yml" 
         end
       end
     end
  end  
end

