# kubeadm-cluster

## Getting started

To make it easy to deploy a kubeadm cluster using Vagrant and Ansible.

## Prerequisites 

- [ ] [Vagrant](https://developer.hashicorp.com/vagrant/downloads) tool installed (latest)
- [ ] [Virtualbox](https://www.virtualbox.org/wiki/Downloads) installed (6 or higher)
- [ ] [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) installed (2.9 or higher) - script provided in the repo called `deploy_ansible.sh`


## High level steps

- [ ] [Clone this repo ]
- [ ] [Review & modify the Vagrantfile]
- [ ] [Start the deployment]
- [ ] [Check the k8s cluster]
- [ ] [Run a sample app]


## Clone this repo

```bash
git clone https://gitlab.com/ytbsakura86/kubeadm-cluster.git
cd kubeadm-cluster
```

## Review & modify the Vagrantfile
The Vagrantfile in the repo is an example of a k8s deployment based on [kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/).
The Vagrant deployment is based on Debian 11 `(default_box = 'generic/debian11')`.

### What I need/can update ?
- List of Nodes :
The variable in the beginning of the Vagrantfile can be updated to match your k8s cluster. If you want to install addtional worker nodes in the cluster, you can just update the list in the **NODES** variable.

- IP addresses of the Nodes :
In the same list you can update the IP addresses to match for your environment. This also depends on the PROVIDER (e.g Virtualbox in this case) which has predefined subnets you can use. Virtualbox has be default the subnet 192.168.56.0/24, so we allocate IP addresses in this range. We are usiong fixed IP addresses to be allocated to the k8s nodes because k8s does not like IP addresses are changing when a node reboots.

- Ports :
In the same list you can update the ports. This port is used by the k8s apiserver, so it is only needed for the master node. Other nodes leave blank or use a dummy. Important is that this must a **FREE** port on the host running Vagrant. In our example we use port 6443 which is the default port used in k8s for the apiserver.

- mapped Ports :
On line 21, you can change the port list that can be mapped fromn the guest VMs to the host. This is important if we need to expose k8s service nodePorts to local ports on the Host. Important is that this must a **FREE** port on the host running Vagrant.

- Resources of the guest VMS :
On line 27 and 30 you can find an example of allocating MEMORY for the guest VMs, depending if the node is a master node or a worker node. For CPU examples, please check the vagrant documentation. Usually a single CPU is not good enough to run a production cluster.

- Ansible provisioner :
Nothing needs to be changed here, it is just for reference. We are using a Ansible provisioner in the Vagrantfile in order to install a k8s cluster master and worker nodes. The Ansible playbooks and related YAML files are located in the directory ansible-playbooks.

## Start the deployment
```bash
vagrant up
```
This will create the guest VMs and run the playbooks to install the k8s cluster. Depending on the available resources on your host server, it might take 10-20 minutes to deploy everything. So be patient ! :-) 

## Check the k8s cluster
```bash
vagrant ssh master
kubectl get nodes
kubectl get po -A
kubectl get svc -A
...
```

## Run a sample app
The following steps is to test if we can deploy an app on this cluster and see if we can reach it from the host.

This is the sample nginx "hello-world" [app](https://kubernetes.io/docs/tutorials/services/connect-applications-service/):

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 1
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: my-nginx
  labels:
    run: my-nginx
spec:
  type: NodePort
  ports:
  - port: 8080
    nodePort: 30009
    targetPort: 80
    protocol: TCP
    name: http
  selector:
    run: my-nginx
```

```bash
kubectl get pod
kubectl get deploy
kubectl get svc 
kubectl apply -f test-app.yaml
curl http://<EXTERNAL-IP>:30009
```

